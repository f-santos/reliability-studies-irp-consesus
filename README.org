#+TITLE: Measures of agreement for categorical and ordinal traits
#+SUBTITLE: IRP Consensus Workshop, Brussels
#+DATE: 16--20 October 2023
#+AUTHOR: Frédéric Santos
#+OPTIONS: _:nil

This public repository provides the slides and various other files for the talk about Cohen's Kappa and friends, given at IRP Consensus Workshop (Brussels, 16--20 October, 2023).

** License
All files available in this repository are licensed under [[https://creativecommons.org/licenses/by-nc-sa/4.0/][Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International]].
